#!/bin/bash

#---------------------------------#
#     RECONFIHURE LOCAL DNS       #
#---------------------------------#

# remove dns container.
docker rm -f koska-dns

# remove mydns image.
docker rmi mydns

# rebuild local image.
docker build -t mydns .

# start DNS container.
docker run --name koska-dns -d --restart=unless-stopped -p 53:53/tcp -p 53:53/udp mydns